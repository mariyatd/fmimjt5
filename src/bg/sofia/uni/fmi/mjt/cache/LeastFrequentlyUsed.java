package bg.sofia.uni.fmi.mjt.cache;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class LeastFrequentlyUsed<K, V> implements Cache<K, V> {
    //TODO node class for use and value, then hashmap K and node
    private HashMap<K, V> cache;
    private HashMap<K, Integer> usesCount;
    private HashMap<Integer, HashSet<K>> counterMappings;
    private int currentMin = 0;
    private final long CAPACITY;
    private long hitCount;
    private long missCount;

    LeastFrequentlyUsed(long capacity) {
        this.cache = new HashMap();
        this.usesCount = new HashMap();
        this.counterMappings = new HashMap();
        this.CAPACITY = capacity;
        this.hitCount = 0;
        this.missCount = 0;
    }

    @Override
    public V get(K key) {
        //TODO fix keyInCache
        if (keyInCache(key)) {
            this.incrementUseCount(key);
            this.hitCount ++;

            return cache.get(key);
        }

        this.missCount ++;
        return null;
    }

    @Override
    public void set(K key, V value) {
        if (key != null && value != null) {
            if (cache.get(key) != null) {
                this.incrementUseCount(key);
            }
            else {
                if (cache.size() >= this.CAPACITY) {
                    this.evict();
                }

                usesCount.put(key, 1);

                //TODO dry
                if (counterMappings.get(1) == null) {
                    counterMappings.put(1, new HashSet());
                }

                counterMappings.get(1).add(key);
                currentMin = 1;
            }

            cache.put(key, value);
        }
    }

    @Override
    public boolean remove(K key) {
        if (keyInCache(key)) {
            //TODO check !keyincache
            int currentUsesCount = usesCount.get(key);

            cache.remove(key);
            usesCount.remove(key);
            counterMappings.get(currentUsesCount).remove(key);

            if (counterMappings.get(currentUsesCount).isEmpty()) {
                if (currentUsesCount == currentMin) {
                    currentMin = this.discoverNewMin();
                }

                counterMappings.remove(currentUsesCount);
            }

            return true;
        }

        return false;
    }

    @Override
    public long size() {
        return cache.size();
    }

    @Override
    public void clear() {
        cache.clear();
        usesCount.clear();
        counterMappings.clear();
        hitCount = 0;
        missCount = 0;
    }

    @Override
    public double getHitRate() {
        if(missCount == 0){
            return 1.0;
        }

        return hitCount / (hitCount + missCount);
    }

    @Override
    public int getUsesCount(K key) {
        if (keyInCache(key)) {
            return usesCount.get(key);
        }

        return 0;
    }

    public boolean keyInCache(K key) {
        if (key == null) {
            throw new RuntimeException("Key mustn't be null");
        }

        if (cache.get(key) != null) {
            return true;
        }
        //TODO same as RR

        return false;
    }

    private K getArbitraryKeyFromLeastUsed(HashSet leastUsed) {
        Iterator<K> iterator = leastUsed.iterator();
        return iterator.next();
    }

    private void incrementUseCount(K key) {
        int oldCounter = usesCount.get(key).intValue();
        int newCounter = oldCounter+1;

        if(counterMappings.get(newCounter)==null) {
            counterMappings.put(newCounter, new HashSet());
        }

        counterMappings.get(newCounter).add(key);
        counterMappings.get(oldCounter).remove(key);

        usesCount.put(key, newCounter);

        if(counterMappings.get(oldCounter).isEmpty()){
            counterMappings.remove(oldCounter);
            if(currentMin == oldCounter) {
                currentMin = newCounter;
            }
        }
        return;
    }

    private void evict() {
        K leastUsedKey = this.getArbitraryKeyFromLeastUsed(counterMappings.get(currentMin));
        counterMappings.get(currentMin).remove(leastUsedKey);
        //TODO make more readable
        if (counterMappings.get(currentMin).isEmpty()) {
            counterMappings.remove(currentMin);
        }

        cache.remove(leastUsedKey);
        usesCount.remove(leastUsedKey);
    }
    private Integer discoverNewMin() {
        Iterator<Integer> iter = counterMappings.keySet().iterator();
        int newMin = iter.next();
        while (iter.hasNext()){
            Integer next = iter.next();
            if(newMin>next) {
                newMin = next;
            }
        }
        return newMin;
    }
}
