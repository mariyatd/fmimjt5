package bg.sofia.uni.fmi.mjt.cache;

import java.util.*;

public class RandomReplacement<K, V> implements Cache<K, V> {
    private Map<K, V> cache;
    //TODO not capitalized, not final
    private final long CAPACITY;
    private long hitCount;
    private long missCount;

    public RandomReplacement(long capacity) {
        this.CAPACITY = capacity;
        this.cache = new HashMap<>();
        //TODO remove hit and miss count
        this.hitCount = 0;
        this.missCount = 0;
    }

    @Override
    public V get(K key) {
        //TODO cache get key local var
        if (cache.get(key) != null) {
            //TODO remove this
            this.hitCount++;
            return cache.get(key);
        }
    //TODO remove this
        this.missCount++;
        return null;
    }

    @Override
    public void set(K key, V value) {
        if(key == null && value == null){
            return;
        }

        //TODO check if already in cache
        if(cache.size() >= CAPACITY) {
            evict();
        }

        cache.put(key, value);
    }

    @Override
    public boolean remove(K key) {
        //TODO use remove, 1 line
        if (cache.get(key) != null) {
            cache.remove(key);
            return true;
        }

        return false;
    }

    @Override
    public long size() {
        return cache.size();
    }

    @Override
    public void clear() {
        cache.clear();
        hitCount = 0;
        missCount = 0;
    }

    @Override
    public double getHitRate() {
        if (missCount == 0) {
            return 1.0;
        }

        return hitCount / (missCount + hitCount);
    }

    @Override
    public int getUsesCount(K key) {
        throw new UnsupportedOperationException("getUsesCount cannot be applied to cache with Random Replacement eviction policy.");
    }

    private void evict(){
        //TODO just remove the first
        List<K> keys = new ArrayList<>(cache.keySet());
        Collections.shuffle(keys);
        cache.remove(keys.get(0));
    }
}
